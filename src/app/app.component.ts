import { NgForm } from '@angular/forms';
import { Component, OnInit} from '@angular/core';

class Cliente{
  nome: string;
  email: string;
  profissao: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{

  cliente: Cliente = new Cliente();

  constructor(){}

  profissoes = [ 'Programador', 'Empresario', 'Outros'];

  salvar(form: NgForm){

    this.cliente.nome = form.value.nome
    this.cliente.email = form.value.email;
    this.cliente.profissao = form.value.profissao;

    form.reset({profissao: ''});



  }

}
