
import { Routes, RouterModule } from '@angular/router';

//Component
import { AppComponent } from './app.component';

//Module
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { HttpModule } from '@angular/http';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';





@NgModule({
  declarations: [
    AppComponent,

 ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,


  ],
  providers: [], //{provide: 'LogPrefixo', useValue: 'Log'} - Para injetar parametro no construtor
  bootstrap: [AppComponent]
})
export class AppModule { }
